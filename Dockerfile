FROM node:19-alpine
LABEL maintainer="Maxime Hamon max@hamon.me"
RUN npm install -g ovh-bill-importer
CMD ovh-bill-importer --dest=/bill --split=month --concurrency=3 --token=/config/token.json
