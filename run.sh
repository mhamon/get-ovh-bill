#!/usr/bin/env bash
set -euo pipefail

_run_dir="$( cd "$( dirname $0 )" && pwd )"
_bill_dir="${_run_dir}/bill"
_uid=$(id -u)
_gid=$(id -g)

[ ! -f ${_run_dir}/docker.properties ] && echo "${_run_dir}/docker.properties" && exit 1
source ${_run_dir}/docker.properties
[ -f ${_run_dir}/config/run.properties ] && . ${_run_dir}/config/run.properties

sudo docker run --rm -v ${_run_dir}/config:/config -v ${_bill_dir}:/bill --user ${_uid}:${_gid} ${_docker_image}:${_docker_image_version}
